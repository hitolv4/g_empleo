<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'genero' => $faker->randomElement($array = array ('masculino','femenino')),
        'grado_de_instruccion'=>$faker->jobTitle,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Jobs::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'titulo' => $faker->jobTitle,
        'descripcion' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'lugar' => $faker->city ,
        'grado_de_instruccion'=>$faker->jobTitle,
        'estado' => $faker->randomElement($array = array ('activo','inactivo')),
        'publicado' => $faker->date($format = 'Y-m-d', $max = 'now'),
    ];
});