<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $fillable = [
    	'titulo','descripcion','lugar','grado_de_instruccion','estado','publicado'
    ];
}
