<?php

namespace App\Http\Controllers;

use App\Jobs;
use Illuminate\Http\Request;

class JobsControler extends Controller
{
    public function index()
    {
    	return Jobs::orderBy('publicado','desc')->get();
    }

    public function store(Request $request)
    {
    	$job=Jobs::create($request->all());
    	return $job;
    }
}
