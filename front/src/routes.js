import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [{
            path: "/conectar",
            component: require('./components/authentication/login.vue'),
            meta: {
                forVisitors: true
            }
        },
        {
            path: "/registro",
            component: require('./components/authentication/register.vue'),
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/feed',
            component: require('./components/feed.vue'),
            meta: {
                forAuth: true
            }
        },
        {
            path: '/crear',
            component: require('./components/jobs/crear.vue'),
            meta: {
                forAuth: true
            }
        },
    ],

    linkActiveClass: 'active',
    mode: 'history',
})





export default router